(in-package :5am)

;; KLUDGE: elephant-tests corrup 5am:finishes macro so it does not
;; work correctly. so we bring it back here, until elephant is fixed.

(defmacro finishes (&body body)
  "Generates a pass if BODY executes to normal completion. In
other words if body does signal, return-from or throw this test
fails."
  `(let ((ok nil))
     (unwind-protect
	 (progn 
	   ,@body
	   (setf ok t))
       (if ok
	   (add-result 'test-passed :test-expr ',body)
           (process-failure
            :reason (format nil "Test didn't finish")
            :test-expr ',body)))))


(in-package :gp-export-tests)

(5am:in-suite* testpm)

;; test with elephant's postmodern backend

;; database should be recreated fresh before running test suite 

(defparameter *testpm-spec* elephant-tests::*testpm-spec*)

(defmacro with-elepm-throwaway-db (&body body)
  `(ele:with-open-store (*testpm-spec*)
    (ele:with-transaction (:always-rollback t)
      ,@body)))

(defmacro elepm-test-create-database (&body body)
  `(with-elepm-throwaway-db
    ,@body
    (5am:finishes (gp-export-elephant:export-current-elephant-database))))

(defmacro elepm-test-verify-database (&body body)
  `(with-elepm-throwaway-db
    (5am:finishes (gp-export:import-dump-file :importer 'gp-export-elephant::elephant-importer))
    ,@body))
    
(5am:test setup-classes
  (5am:finishes
    (progn
      (ele:defpclass thingie ()
	((islot :accessor islot-of :initarg :islot :index t)
	 (uslot :accessor uslot-of :initarg :uslot)))
      (defstruct teststr1 
	xo yo)
      (defstruct (teststr2 (:type list))
	xo yo)
      (defclass testclass ()
	((islot :accessor islot-of :initarg :islot)
	 (uslot :accessor uslot-of :initarg :uslot)))
      (defclass emptyclass () ()))))
	
(5am:test (simple :depends-on setup-classes)
  (elepm-test-create-database (make-instance 'thingie :islot 11 :uslot "sha"))
  (elepm-test-verify-database 
   (let ((thingies (ele:get-instances-by-class 'thingie)))
      (5am:is (= 1 (length thingies)))
      (5am:is (= 11 (islot-of (first thingies))))
      (5am:is (equal "sha" (uslot-of (first thingies)))))))

;;warning: might now work correctly in elephant 0.9
(5am:test (indices-mixed :depends-on simple)
  (elepm-test-create-database 
    (make-instance 'thingie :islot 11 :uslot "sha")
    (make-instance 'thingie :islot "sha" :uslot 11))
  (elepm-test-verify-database 
   (let ((thingies (ele:get-instances-by-class 'thingie)))
      (5am:is (= 2 (length thingies)))
      (5am:is (= 1 (length (ele:get-instances-by-value 'thingie 'islot 11))))
      (5am:is (= 1 (length (ele:get-instances-by-value 'thingie 'islot "sha"))))
      (5am:is (= 11 (uslot-of (ele:get-instance-by-value 'thingie 'islot "sha"))))
      (5am:is (equal "sha" (uslot-of (ele:get-instance-by-value 'thingie 'islot 11)))))))
  
(defmacro value-equality-test (name value-form &key depends-on predicate)
  `(5am:test (,name :depends-on ,depends-on)
    (let ((the-value ,value-form))
      (elepm-test-create-database 
	(make-instance 'thingie :islot 1 :uslot the-value))
      (elepm-test-verify-database 
	(let (thingie)
	  (5am:finishes (setf thingie (first (ele:get-instances-by-class 'thingie))))
	  (5am:is (,predicate the-value (uslot-of thingie))))))))

;; includes UNICODE
(value-equality-test hairy-strings 
		       (let ((really-hairy-string (copy-seq "abcabc\"'asdasd'  \\ \\ ))))) (((( ")))
			 #+sbcl (setf (char really-hairy-string 0)
				      #\CYRILLIC_CAPITAL_LETTER_A_WITH_DIAERESIS
				      (char really-hairy-string 1)
				      #\ARABIC_LETTER_BEH)
			 (setf (char really-hairy-string 2) #\tab)
			 really-hairy-string)
		       :depends-on simple
		       :predicate equal)

(value-equality-test list '(1 2 3 () ((123132)) "abc") :depends-on simple :predicate equal)
(value-equality-test list-empty () :depends-on list :predicate equal)

(value-equality-test numbers (list 0.013213 -0.123213 137/46 1.e-16 
				  -43525724857473577763345)
		     :depends-on list :predicate equal)

(value-equality-test symbols (list 'cl:defun 'value-equality-test
				   '|ggHHttHH| 'gghhtthh
				   'gp-export::serializable-slots
				   :package)
		     :depends-on list :predicate equal)

(value-equality-test structs (list (make-teststr1 :xo 1 :yo 2)
				   (make-teststr2 :xo "a" :yo 15))
		     :depends-on list :predicate equalp)

(defgeneric object-compare (a b))
(defmethod object-compare (a b) nil)
(defmethod object-compare ((a null) (b null)) t)
(defmethod object-compare ((a cons) (b cons))
  (and (object-compare (first a) (first b))
       (object-compare (rest a) (rest b))))
(defmethod object-compare ((a standard-object) (b standard-object))
  (and (eq (class-of a) (class-of b))
       (loop for slot-name in (gp-serialization::serializable-slots a)
	     always (or (and (slot-boundp a slot-name)
			     (slot-boundp b slot-name)
			     (equal (slot-value a slot-name)
				    (slot-value b slot-name)))
			(and (not (slot-boundp a slot-name))
			     (not (slot-boundp b slot-name)))))))

(value-equality-test objects (list (make-instance 'testclass :islot "asdas" :uslot 15)
				   (make-instance 'testclass :uslot 37)
				   (make-instance 'testclass)
				   (make-instance 'emptyclass))
		     :depends-on list
		     :predicate object-compare)
				  

		     

(value-equality-test ht-simple
		     (let ((ht (make-hash-table :test 'equalp)))
		       (setf (gethash "bu-go" ht) "ga"
			     (gethash 3 ht) "11"
			     (gethash (list 123 123 123 "bac") ht) (list 123 1433434 32 432 42))
		       ht)
		       :depends-on list
		       :predicate equalp)


(5am:test (btree-simple :depends-on simple)
  (elepm-test-create-database 
    (let ((bt1 (ele:make-btree))
	  (bt2 (ele:make-btree)))
      (make-instance 'thingie :islot bt1 :uslot bt2)
      (setf (ele:get-value "abc" bt1) 13)
      (setf (ele:get-value "def" bt1) 17)
      (setf (ele:get-value 17 bt2) "abc")
      (setf (ele:get-value 13 bt2) "def")
      (setf (ele:get-value 11 bt2) "ghhh~~~~~()()")))
  (elepm-test-verify-database 
    (let (thingie)
      (5am:finishes (setf thingie (first (ele:get-instances-by-class 'thingie))))
      (let ((bt1 (islot-of thingie))
	    (bt2 (uslot-of thingie)))
	(5am:is-true (typep bt1 'ele:btree))
	(5am:is-true (typep bt2 'ele:btree))
	(5am:is (= 2 (length (ele:map-btree #'ele::identity2 bt1 :collect t))))
	(5am:is (= 3 (length (ele:map-btree #'ele::identity2 bt2 :collect t))))
	(5am:is (= 13 (ele:get-value "abc" bt1)))
	(5am:is (= 17 (ele:get-value "def" bt1)))
	(5am:is (equal "def" (ele:get-value 13 bt2)))
	(5am:is (equal "ghhh~~~~~()()" (ele:get-value 11 bt2)))))))

(5am:test (btree-nested :depends-on btree-simple)
    (elepm-test-create-database 
      (let* ((bt1 (ele:make-btree))
	     (bt2 (ele:make-btree))
	     (thingie1 (make-instance 'thingie :islot 1 :uslot (list bt1))))
	(setf (ele:get-value "next" bt1) bt2
	      (ele:get-value "abc" bt2) 1337)))
    (elepm-test-verify-database 
      (let (thingie)
	(5am:finishes (setf thingie (first (ele:get-instances-by-class 'thingie))))
	(5am:is-true (consp (uslot-of thingie)))
	(let ((bt1 (first (uslot-of thingie))))
	  (5am:is-true (typep bt1 'ele:btree))
	  (5am:is-true (ele:existsp "next" bt1))
	  (5am:is-false (ele:existsp "abc" bt1))
	  (let ((bt2 (ele:get-value "next" bt1)))
	    (5am:is-true (typep bt2 'ele:btree))
	    (5am:is-true (ele:existsp "abc" bt2))
	    (5am:is (= 1337 (ele:get-value "abc" bt2)))
	    (5am:is-false (ele:existsp "next" bt2)))))))

(5am:test (btree-nested-circular-knot :depends-on btree-nested)
  (elepm-test-create-database 
    (let* ((bt1 (ele:make-btree))
	   (bt2 (ele:make-btree))
	   (thingie1 (make-instance 'thingie :islot 1 :uslot bt1))
	   (thingie2 (make-instance 'thingie :islot 2 :uslot bt2)))
      ;; cross-link them all
      (setf (ele:get-value "sibling" bt1) bt2
	    (ele:get-value "parent" bt1) thingie1
	    (ele:get-value "aunt" bt1) thingie2
	    (ele:get-value "name" bt1) "bt1"

	    (ele:get-value "sibling" bt2) bt1
	    (ele:get-value "parent" bt2) thingie2
	    (ele:get-value "aunt" bt2) thingie1
	    (ele:get-value "name" bt2) "bt2")))
  (elepm-test-verify-database 
    (let (thingie1 thingie2)
      (5am:finishes (setf (values thingie1 thingie2)
			  (values-list (ele:get-instances-by-class 'thingie)))
		    (5am:is-true thingie1)
		    (5am:is-true thingie2)
		    (when (= (islot-of thingie1) 2)
		      (rotatef thingie1 thingie2))
		    (5am:is (= 1 (islot-of thingie1)))
		    (5am:is (= 2 (islot-of thingie2))))

      (let ((bt1 (uslot-of thingie1))
	    (bt2 (uslot-of thingie2)))

	(5am:is (= 4 (length (ele:map-btree #'ele::identity2 bt1 :collect t))))
	(5am:is (= 4 (length (ele:map-btree #'ele::identity2 bt2 :collect t))))
		
	(5am:is (equal (ele:get-value "name" bt1) "bt1"))
	(5am:is (equal (ele:get-value "parent" bt1) thingie1))
	(5am:is (equal (ele:get-value "sibling" bt1) bt2))
	(5am:is (equal (ele:get-value "aunt" bt1) thingie2))

	(5am:is (equal (ele:get-value "name" bt2) "bt2"))
	(5am:is (equal (ele:get-value "parent" bt2) thingie2))
	(5am:is (equal (ele:get-value "sibling" bt2) bt1))
	(5am:is (equal (ele:get-value "aunt" bt2) thingie1))))))


(5am:in-suite* testpm-upgrade-0.9-1.0)

;; we cannot automatically run tests on database upgrade.
;; so it works like this: you run btree-nested-circular-knot-export
;; in old version, then load a new version and run btree-nested-circular-knot-import.
;; if it passes, then it works. backup.sexp should be preserved between runs, of course.
;; don't forget you need different databases for different versions.

(5am:test (btree-nested-circular-knot-export :depends-on setup-classes)
  (elepm-test-create-database 
    (let* ((bt1 (ele:make-btree))
	   (bt2 (ele:make-btree))
	   (thingie1 (make-instance 'thingie :islot 1 :uslot bt1))
	   (thingie2 (make-instance 'thingie :islot 2 :uslot bt2)))
      ;; cross-link them all
      (setf (ele:get-value "sibling" bt1) bt2
	    (ele:get-value "parent" bt1) thingie1
	    (ele:get-value "aunt" bt1) thingie2
	    (ele:get-value "name" bt1) "bt1"

	    (ele:get-value "sibling" bt2) bt1
	    (ele:get-value "parent" bt2) thingie2
	    (ele:get-value "aunt" bt2) thingie1
	    (ele:get-value "name" bt2) "bt2"))))


(5am:test (btree-nested-circular-knot-import  :depends-on setup-classes)
  (elepm-test-verify-database 
    (let (thingie1 thingie2)
      (5am:finishes (setf (values thingie1 thingie2)
			  (values-list (ele:get-instances-by-class 'thingie)))
		    (5am:is-true thingie1)
		    (5am:is-true thingie2)
		    (when (= (islot-of thingie1) 2)
		      (rotatef thingie1 thingie2))
		    (5am:is (= 1 (islot-of thingie1)))
		    (5am:is (= 2 (islot-of thingie2))))

      (let ((bt1 (uslot-of thingie1))
	    (bt2 (uslot-of thingie2)))

	(5am:is (= 4 (length (ele:map-btree #'ele::identity2 bt1 :collect t))))
	(5am:is (= 4 (length (ele:map-btree #'ele::identity2 bt2 :collect t))))
		
	(5am:is (equal (ele:get-value "name" bt1) "bt1"))
	(5am:is (equal (ele:get-value "parent" bt1) thingie1))
	(5am:is (equal (ele:get-value "sibling" bt1) bt2))
	(5am:is (equal (ele:get-value "aunt" bt1) thingie2))

	(5am:is (equal (ele:get-value "name" bt2) "bt2"))
	(5am:is (equal (ele:get-value "parent" bt2) thingie2))
	(5am:is (equal (ele:get-value "sibling" bt2) bt1))
	(5am:is (equal (ele:get-value "aunt" bt2) thingie1))))))