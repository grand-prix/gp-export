# lob-dump
Name? gp-export, lob-dump clob-dump
lob-dump is lisp objects dump, a way to export lisp objects to a file and restore.

Goals:
 
* Compatible with several lisp object databases (elpehant, rucksack)
* Can serialize large databases (that don't fit main memory)
* Easy to use interface
* Easily tweaked by the user. 
You might (I did) need to export/import things in a certain order, skip certain instances,
or anything else that is application specific. We want lots of generic 
functions that are easy to hook into!

Status:
* First version for elephant
* Flexible API: Hooks in place to determine export order and other things.

TODO:
* Bookkeeping of serialized objects in a file. They might not fit into main memory!
The idea that pops up is a simple file with object-ids (each database has one, right?),
sorted in records of fixed size. 
That makes it easy to search it by binary search, which is efficient enough.
New objects can be saved in memory (say 100). Then they can be sorted in memory,
flushed to a new file. Then a simple merge of the two files to update the object-id-file.
* When doing aboved we might just as well do our own serializer instead of 
s-serialization. It also reduces dependencies.
* Implementation for rucksack
* Export from rucksack to elephant and vice versa.
* Testcases
* Documentation
* XML. If you are a consultant xml is easier to sell in than sexp. So we 
could add a useless layer after serializing to sexp to convert that representation
to XML and vice versa.
* Alternative to XML and sexp: pyx, line-oriented xml. easier and better with line tools like diff:
http://gnosis.cx/publish/programming/xml_matters_17.html
* More ideas?

