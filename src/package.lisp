;;; -*- lisp -*-

;; It is possible that some symbols are forgotten to be exported, add when needed.

(defpackage :gp-serialization 
  (:use :common-lisp)
  (:export #:serialize-sexp
	   #:serialization-state
	   #:serialize-sexp-internal 
	   #:get-serializable-slots
	   #:serialize-sexp-internal-meta
	   #:deserialize-sexp
	   #:deserialize-sexp-internal
	   #:make-serialization-state
	   
	   ;; eww
	   #:get-hashtable))
  

(defpackage :gp-export
  (:use :common-lisp)
  (:export #:lg
           #:exporter
           #:standard-exporter
           #:step-one-initialize
           #:step-two-describe-export
           #:begin-export
           #:exporter-modify-instance-info
	   #:make-instance-info
	   #:make-real-instance
           #:add-instance
           #:end-export
	   #:read-export-info

           #:importer
           #:standard-importer
           
           ;; Hooks
           #:import-instance-p
           #:deserialize-instance-info
           #:modify-instance-info
           #:repeat-import-p
	   #:modify-object-serializable-slots
	   #:serialize-sexp-meta
           #:allocate-real-instance
           #:modify-real-instance
           #:export-info
           #:import-dump-file
	   #:do-import
	   #:do-import-stream))