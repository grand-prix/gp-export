;;; -*- lisp -*-

(in-package  :gp-export)

;; -----------------------------------------------------------------------------
;;                               Logging
;; -----------------------------------------------------------------------------

(defun make-simple-logger (stream)
  (lambda (msg &rest args)
    (apply #'format stream msg args)
    (terpri stream)))


(defparameter *logger* (make-simple-logger *standard-output*))

(defun lg (&rest args)
  (apply *logger* args))

(defun lg-separator ()
  (lg "-----------------------------------------------------------"))

;; -----------------------------------------------------------------------------
;;                          Export info
;; -----------------------------------------------------------------------------

(defclass export-info ()
    ((framework-name :initarg :framework-name)
     (exporter-class :accessor exporter-class-of)
     (export-time :initform (get-universal-time)))
  (:documentation "This class contains information about the export.
Used both when exporting and importing. Use subclasses of this object to
specialize import functionality"))

(defmethod print-object ((export-info export-info) stream)
  (print-unreadable-object (export-info stream :type t)
    (with-slots (framework-name exporter-class export-time)
        export-info
      (multiple-value-bind (second minute hour date month year day daylight-p zone)
          (decode-universal-time export-time)
        (declare (ignore second minute daylight-p zone))
        (format stream "Framework ~a exported ~a-~a-~a:~a:~a YYYY-MM-DD:HH:MM  exporter-class ~a"
                framework-name
                year month day hour minute
                exporter-class)))))

;; -----------------------------------------------------------------------------
;;                          Standard export
;; -----------------------------------------------------------------------------

#| Export begins with writing export-info object data which described database
being exported (this can be used to determine if import needs any fix-ups), then
it goes through all instances it finds in database and writes them one by one
into the dump file.

Each instance found is fed to add-instance function, which converts it to sexpr 
via make-instance-info and writes it into the dump file.

make-instance-info serializes instance in a recursive way using gp-serialization
serializer (fork of s-serialization from cl-prevalence). (Note: make-instance-info
is called only for top-level objects, nested ones are processed through gp-serialization.)
 Serialization can be customized via few hook function:

  (modify-object-serializable-slots exporter object slots)
    -- can be used to remove (or add?) some object slots from serialization,

  (serialize-sexp-meta exporter instance stream serialization-state)
    -- can be used to serialize additional information, it should be written
    into stream in plist format, e.g. ":METAINFO1 (a b c) :METAINFO2 (1 2 3)"

These hooks are called for all objects, both top-level and nested ones.

Once sexp is serialized, it can be processed via exporter-modify-instance-info
function, but it is pretty cumbersome, since it needs to process raw tree and
do recursion itself, so it might be eliminated in future, this function is 
deprecated.
|#

(defclass exporter ()
  ())

(defgeneric step-one-initialize (exporter)
  (:method :before ((exporter exporter))
    (lg "Initializing (step-one-initialize)")))

(defgeneric step-two-describe-export (exporter export-info)
  (:method :before ((exporter exporter) export-info)
     (lg "First step, (step-one-describe-export)")))

(defgeneric begin-export (exporter)
  (:method :before ((exporter exporter))
    (lg "Beginning export (initialize-export)")))

(defgeneric make-instance-info (export instance)
  (:documentation "make instance info to be written into stream"))

(defgeneric add-instance (exporter instance)
  (:method :before ((exporter exporter) instance)
    (lg "Adding instance ~a to export." instance)))

(defgeneric exporter-modify-instance-info (exporter instance-info)
  (:documentation "Modify instance-info structure or wipe it off completely. (DEPRECATED)")
  (:method (exporter instance-info)
	   (lg "No modification to instance (exporter-modify-instance-info")
	   instance-info))

(defgeneric modify-object-serializable-slots (exporter object slots)
  (:documentation "Hook to filter serializable slots if necessary.")
  (:method (exporter object slots)
	   (declare (ignore exporter object))
	   slots))


(defgeneric end-export (exporter)
  (:method :before ((exporter exporter))
    (lg "Finising export (end-export)"))
  (:method :before ((exporter exporter))
    (lg "Finished export (end-export)")))

;; -----------------------------------------------------------------------------
;;                     sexp-based standard-exporter
;; -----------------------------------------------------------------------------

(defparameter *default-export-import-pathname*
  (make-pathname :name "backup"
                 :type "sexp"
                 :defaults *load-truename*))

;; a customized serialization-state class used to add some hooks
(defclass exporter-serialization-state (gp-serialization:serialization-state)
  ((exporter :accessor exporter-of :initarg :exporter)))


(defclass standard-exporter (exporter)
  ((pathname :accessor pathname-of :initarg :export-file-path
             :initform *default-export-import-pathname*)
   (log-to-file-p :initarg :log-to-file-p :initform t :accessor log-to-file-p-of)
   (logstream :accessor logstream-of :initform nil)
   (backup-logger :accessor backup-logger-of)
   (stream :accessor stream-of)
   (external-format :accessor external-format-of :initarg :external-format :initform :default)
   (serialization-state :accessor serialization-state-of)))

(defgeneric serialize-sexp-meta (exporter object stream ss)
  (:documentation "A hook to serialize additional information for an object.")
  (:method (exporter object stream ss)
	    (declare (ignore exporter object stream ss))))
	    

(defmethod step-one-initialize ((ex standard-exporter))
  (setf (stream-of ex) (open (pathname-of ex)
                             :direction :output
                             :if-exists :supersede
                             :if-does-not-exist :create
                             :external-format (external-format-of ex)))
  (when (log-to-file-p-of ex)
    (let ((logstream (open (make-pathname :type "log" :defaults (pathname-of ex)) 
                             :direction :output
                             :if-exists :supersede
                             :if-does-not-exist :create
                             :external-format (external-format-of ex))))
      (setf (logstream-of ex) logstream)
      (setf (backup-logger-of ex) *logger*)
      (setf *logger* (make-simple-logger logstream))))
  (lg "Exporing to ~a" (pathname-of ex)))

(defmethod step-two-describe-export ((ex standard-exporter)
                                     (info export-info))
  (setf (exporter-class-of info) (class-name (class-of ex)))
  (gp-serialization:serialize-sexp info (stream-of ex)))

(defmethod begin-export ((ex standard-exporter))
  (setf (serialization-state-of ex) (make-instance 'exporter-serialization-state :exporter ex)))

(defmethod make-instance-info ((ex standard-exporter) instance)
  (let ((*package* (find-package :cl-user)))
    (let* ((temp-string (with-output-to-string (stream)
					       (gp-serialization:serialize-sexp-internal 
						instance
						stream
						(serialization-state-of ex))))
           (instance-info (read-from-string temp-string)))
      (exporter-modify-instance-info ex instance-info))))

(defmethod gp-serialization:get-serializable-slots ((ss exporter-serialization-state) object)
  "give exporter a chance to modify slots on per object basis."
  (modify-object-serializable-slots (exporter-of ss) object (call-next-method)))

(defmethod gp-serialization:serialize-sexp-internal-meta (object stream (ss exporter-serialization-state))
  (serialize-sexp-meta (exporter-of ss) object stream ss))

(defmethod add-instance ((ex standard-exporter) instance)
  ;; Why the -internal version? It does not reset the serialization state.
  (terpri (stream-of ex))
  (terpri (stream-of ex))
  (let* ((*package* (find-package :cl-user))
	 (instance-info (make-instance-info ex instance)))
      (if instance-info
          (prin1 instance-info (stream-of ex))
          (lg "Instance not exported (thanks to exporter-modify-instance-info)"))))

(defmethod end-export ((ex standard-exporter))
  (close (stream-of ex))
  (when (logstream-of ex)
    (close (logstream-of ex))
    (setf *logger* (backup-logger-of ex))))

;; -----------------------------------------------------------------------------
;;                                 Importing
;; -----------------------------------------------------------------------------

#|

Importer basically goes through the dump file, reading instance infos one by
one and instantiating them. Instantiation is a rather complex process, it has
multiple hooks that allow customize/transform importing, load some additional 
data etc.

First record in the dump file is the export-info which describes database that
was exported. We could choose importer automatically from this export info, but
we haven't implemented it yet. (TODO: implement automatic choice of importer.)

Then instance s-expressions are read one by one via read-instance-info function.
Each instance is then instantiated calling:

  (make-real-instance importer export instance-info)

(This is the method that should be used for all importer job to convert sexps
to instances.)
Once invoked, (standard implementation) it first checks if instance should
be imported:
  
  (import-instance-p importer export instance-info)

Then instance-info can be processed via modify-instance-info hook function.
(This hook is deprecated because it operates on raw sexp data and thus needs
to implement recursive descent itself. It ain't cool to implement this recursive
descent in each function.) Standard implementation does nothing returning
instance-info.

Then instance info is fed to deserialize-instance-info function. This function
essentially calls gp-serialization:deserialize-sexp-internal (which does recursive
deserialization) passing it two hooks which affect object deserialization: one
is used to create fresh objects, another to process objects when they are deserialized.
From importer's front-end perspectives, these hooks are 

   (allocate-real-instance importer export class-name instance-info)

and 

   (modify-real-instance importer export instance instance-info)

First is called instead of make-instance, default implementation just calls
(make-instance 'class-name), but a custmized one can be used to map classes, 
possibly using raw instance-info.

modify-real-instance is called after object is deserialized, it gets raw
instance-info and can extract some additional information from it.

Note that allocate-real-instance and modify-real-instance hooks are called
from recursive deserialization process, thus they will be called not only
for top-level instances but also for nested objects etc.

|#

(defun import-dump-file (&key (file *default-export-import-pathname*)
			      (importer 'standard-importer))
  "The main entry function"
  (if (equalp (pathname-type file)
              "sexp")
      (import-sexp file importer)
      (error "Unknown file format ~a" file)))

(defclass importer ()
  ())

(defgeneric read-export-info (importer stream)
  (:documentation "Returns an export-info object that describes the files contents."))

(defgeneric read-instance-info (importer stream)
  (:documentation "Returns instance-info, return nil when end of file."))

(defgeneric deserialize-instance-info (importer export instance-info hook)
  (:documentation "Create a real live object from the instance-info"))

(defgeneric import-instance-p (importer export instance-info)
  (:documentation "Return t if the instance shall be imported")
  (:method ((importer importer) (export t) (instance-info t))
    "Default is to import every instance"
    t))

(defgeneric modify-instance-info (importer export instance-info)
  (:documentation "Hook to modify instance-info before creating real object. DEPRECATED")
  (:method ((importer importer) (export t) (instance-info t))
    instance-info))

(defgeneric allocate-real-instance (importer export class-name instance-info)
  (:documentation "Default implementation just calls (make-instance class-name),
  importers can hook this function to map class names etc.")
  (:method ((importer importer) (export t) class-name sexp)
	   (declare (ignore importer export sexp))
	   (make-instance class-name))) ;; TODO: should be allocate-instance?

(defgeneric modify-real-instance (importer export instance instance-info)
  (:documentation "Hook to modify newly created instance. Modification can take in account
original (unmodified) instance info")
  (:method ((importer importer) (export t) (instance t) (original-instance-info t))
    instance))

(defgeneric make-real-instance (importer export instance-info)
  (:documentation "Make a real object from instance-info")
  (:method ((importer importer) (export t) (instance-info t))
    (when (import-instance-p importer export instance-info)
      (flet ((process-instance (i info)
	       (modify-real-instance importer export i info)))
	 (deserialize-instance-info importer export
				    (modify-instance-info importer
							  export
							  instance-info)
				    #'process-instance)))))

(defgeneric repeat-import-p (importer export)
  (:documentation "t if the repeate import the same data. If you want several passes of import.
For example if you want to import certain classes before others, then implement several passes
and filter the objects using import-instance-p")
  (:method ((importer importer) (export t))
    "No repeat by default"
    nil))

(defgeneric import-instances (importer exporter stream)
  (:documentation "import main function"))


;; -----------------------------------------------------------------------------
;;                             Standard sexp based importer
;; -----------------------------------------------------------------------------

(defun import-sexp (file importer-class)
  (do-import file (make-instance importer-class)))

(defclass standard-importer (importer)
  ((deserialization-state :accessor deserialization-state-of)))

(defmethod read-export-info ((importer standard-importer) (stream stream))
  (gp-serialization:deserialize-sexp stream))

(defmethod read-instance-info ((importer standard-importer) (stream stream))
  (let ((*package* (find-package :cl-user)))
    (read stream nil nil)))

(defmethod deserialize-instance-info ((importer standard-importer) (export t) instance-info hook)
  (gp-serialization:deserialize-sexp-internal instance-info
			     (gp-serialization:get-hashtable 
			      (deserialization-state-of importer))
			     :make-instance (lambda (class sexp) (allocate-real-instance importer export class sexp))
			     :post-process-object-hook hook))

(defmethod import-instances :before ((importer standard-importer) (export t) (stream t))
  (setf (deserialization-state-of importer) (gp-serialization:make-serialization-state)))

(defmethod instance-info-class-name ((importer standard-importer) instance-info)
  (let* ((cp (and (not (eq :ref (car instance-info)))
                  (position :class instance-info)))
         (key (if cp
                  (elt instance-info (1+ cp))
                  'reference-to-previously-created-object)))
    key))

;; -----------------------------------------------------------------------------
;;                             Common import
;; -----------------------------------------------------------------------------

(defmethod import-instances ((importer standard-importer) 
                             (export export-info)
                             (stream stream))
  (loop do
       (let ((fp-start (file-position stream))
             (summary (make-hash-table)))
         (flet ((add-summary-info (key )
                  (setf (gethash key summary)
                        (1+ (gethash key summary 0))))
                (show-summary ()
                  (lg-separator)
                  (lg "Summary of import:")
                  (maphash (lambda (class-name count)
                             (lg "~a instances of ~a" count class-name))
                           summary)))
           (loop for instance-info = (read-instance-info importer stream)
              while instance-info do
                (progn
                  (lg "Reading instance: ~a" instance-info)
                  (let ((instance (make-real-instance importer export instance-info)))
                    (lg "Created instance ~S" instance)
                    (add-summary-info (class-name (class-of instance))))))
           (show-summary))
         (file-position stream fp-start))
       while (repeat-import-p importer export)))

;; It can be convenient to keep the last important objects in global variables
;; To aid inspection and debugging

(defvar *export-info* nil)
(defvar *importer* nil)

(defun do-import-stream (stream importer)
  (lg "Beginning import")  
  (setf *export-info* (read-export-info importer stream))
  (setf *importer* importer)
  (lg "The export is: ~S" *export-info*)
  (import-instances importer *export-info* stream)
  (lg "Import finished."))

(defun do-import (file importer)
  (with-open-file (stream file)
      (do-import-stream stream importer)))

