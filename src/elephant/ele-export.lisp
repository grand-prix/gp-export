;;; -*- lisp -*-

(in-package :gp-export-elephant)

;; -----------------------------------------------------------------------------
;;      Export from Elephant, specialize methods on elephant-export-info class
;; -----------------------------------------------------------------------------

(defclass elephant-export-info (export-info)
  ((db-version :initarg :db-version)
   (spec :initarg :spec)
   (ele-backend :initarg :ele-backend))
  (:default-initargs :framework-name 'elephant))

(defclass elephant-exporter (standard-exporter)
  ((slots-to-remove :accessor slots-to-remove)))

(defclass elephant-importer (standard-importer)
  ((class-map :accessor class-map-of :initform nil)))

(defparameter *elephant-0.9-slots-to-remove* "(elephant::%oid elephant::dbconnection-spec-pst)")

(defparameter *elephant-1.0-slots-to-remove* "(elephant::oid elephant::spec)")

(defparameter *db-postmodern-0.9-slots-to-remove* "(db-postmodern::dbtable db-postmodern::key-type db-postmodern::value-type db-postmodern::indices DB-POSTMODERN::QUERIES)")

(defparameter *db-postmodern-0.9-to-1.0-class-mapping* "((db-postmodern::pm-btree db-postmodern::pm-btree-wrapper) (db-postmodern::pm-btree-index db-postmodern::pm-btree-index-wrapper) (db-postmodern::pm-indexed-btree db-postmodern::pm-indexed-btree-wrapper))")

(defmethod allocate-real-instance ((imp elephant-importer) export class-name instance-info)
  (declare (ignore export instance-info))
  (let* ((class-map (class-map-of imp))
	 (new-class-name (and class-map
			      (second (assoc class-name class-map)))))
    (call-next-method imp export (or new-class-name class-name) instance-info)))

(defmethod modify-object-serializable-slots ((ex elephant-exporter) (object ele:persistent) slots)
  (set-difference slots (slots-to-remove ex)))

(defmethod serialize-sexp-meta ((ex elephant-exporter) (bt elephant:btree) stream ss)
  ;; serialize btree contents
  (princ " :BTREE-CONTENT (" stream)
  (ele:map-btree (lambda (k v)
		   (write-string " (" stream)
		   (gp-serialization:serialize-sexp-internal k stream ss)
		   (write-string " . " stream)
		   (gp-serialization:serialize-sexp-internal v stream ss)
		   (write-string ")" stream))
		 bt)
  (princ " ) " stream))


(defmethod modify-real-instance ((imp elephant-importer) export (bt elephant:btree) original-instance-info)
  ;; read btree contents
  (loop for (k . v) in (getf original-instance-info :btree-content)
	for k-imp = (make-real-instance imp export k)
	for v-imp = (make-real-instance imp export v)
	do (setf (elephant:get-value k-imp bt) v-imp)))

(defgeneric classes-export-order (exporter class-list)
  (:documentation "Hook if you want to export classes in a certain order before exporting")
  (:method ((exporter elephant-exporter) class-list)
    class-list))

(defun export-current-elephant-database (&key exporter export-info)
  "Assumes an open store"
  (export-database (or exporter (make-instance 'elephant-exporter))
                   (or export-info (make-instance 'elephant-export-info))))

(defmethod step-one-initialize :after ((exporter elephant-exporter))
  (let ((dbver (elephant::database-version elephant:*store-controller*)))
    ;; remove specific slots, specific to elephant version
    (cond ((equal (butlast dbver) '(0 9))
	   (setf (slots-to-remove exporter) (read-from-string *elephant-0.9-slots-to-remove*)))
	  ((equal (butlast dbver) '(1 0))
	   (setf (slots-to-remove exporter) (read-from-string *elephant-1.0-slots-to-remove*)))
	  (t (warn "Unsupported elephant version.")))
    ;; check if it is db-postmodern
    (when (eq (car (elephant::controller-spec elephant:*store-controller*)) :postmodern)
      ;; remove db-postmodern-specific slots
      (setf (slots-to-remove exporter)
	    (append (slots-to-remove exporter)
		    (cond ((equal (butlast dbver) '(0 9))
			   (read-from-string *db-postmodern-0.9-slots-to-remove*))
			  ((equal (butlast dbver) '(1 0))
			   ())))))))


(defmethod export-database ((exporter elephant-exporter)(export-info elephant-export-info))
  "Assumes an open store"
  (step-one-initialize exporter)
  
  (setf (slot-value export-info 'db-version) (elephant::database-version elephant:*store-controller*))
  (setf (slot-value export-info 'spec) (elephant::controller-spec elephant:*store-controller*))
  (setf (slot-value export-info 'ele-backend) (car (elephant::controller-spec elephant:*store-controller*)))
  
  (step-two-describe-export exporter export-info)
  (begin-export exporter)
  (let (persistent-classes)
    (if (fboundp 'elephant::controller-class-root)
	;; elephant 0.9.x
	(elephant:map-btree (lambda (k v)
			      (declare (ignore v)) (push k persistent-classes))
			    (elephant::controller-class-root elephant:*store-controller*))
	;; elephant 1.0.x 
	(let ((ht (make-hash-table)))
	  (elephant:map-btree (lambda (cname schema)
				(declare (ignore v))
				(when (find-class cname nil)
				  (setf (gethash cname ht) t)))
			      (elephant::controller-schema-name-index elephant:*store-controller*))
	  (loop for cn being each hash-key of ht
		do (push cn persistent-classes))))

    (lg "~a classes to export.~%" (length persistent-classes))
    (loop for class-name in (classes-export-order exporter persistent-classes)
       for instances = (elephant:get-instances-by-class class-name)
       do 
       (lg "Exporting ~a instances of ~a~%" (length instances) class-name)
       (loop for instance in instances do
            (add-instance exporter instance)))
    (end-export exporter)))

(defmethod read-export-info ((importer elephant-importer) (stream stream))
  ;; autodetect conversions according to export-info.
  (let ((ei (call-next-method)))
    (when (typep ei 'elephant-export-info)
      ;; detect if it is 0.9 -> 1.0 postmodern upgrade
      (when (and (eq (car (elephant::controller-spec elephant:*store-controller*)) :postmodern)
		 (eq (slot-value ei 'ele-backend) :postmodern)
		 (equal (butlast (slot-value ei 'db-version)) '(0 9))
		 (equal (butlast (elephant::database-version elephant:*store-controller*)) '(1 0)))
	(setf (class-map-of importer) (read-from-string *db-postmodern-0.9-to-1.0-class-mapping*))))
    ei))