;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-

(defpackage :gp-export-elephant-system
    (:use :common-lisp :asdf))

(in-package :gp-export-elephant-system)

(defsystem :gp-export-elephant
  :components
  ((:module :src
	    :components
	    ((:module :elephant
                      :serial t
                      :components
                      ((:file "package")
                       (:file "ele-export"))))))
  :depends-on (:elephant
              :gp-export))
