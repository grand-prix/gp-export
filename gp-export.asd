;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-

(defpackage :gp-export-system
    (:use :common-lisp :asdf))

(in-package :gp-export-system)

(defsystem :gp-export
  :components
  ((:module :src
            :serial t
	    :components
	    ((:file "package")
	     (:file "serializer")
             (:file "transfer"))))
  :depends-on (:cl-prevalence))


